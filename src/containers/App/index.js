import React, { Component } from "react";
import Board from "../Board/Board";
import { Modal, Button, Layout, Menu, Input } from "antd";

const { Header, Content, Footer } = Layout;
class App extends Component {
  state = {
    rowCount: 3,
    colCount: 3,
    turn: "blue",
    blueScore: 0,
    redScore: 0,
    showModal: false,
    start: false,
    blueName: "",
    redName: "",
    isGameFinish: false
  };

  countScoreHandler = turn => {
    this.setState(state => {
      const { blueScore, redScore, rowCount, colCount } = state;
      let isGameFinish = false;
      if (blueScore + redScore + 1 === rowCount * colCount) {
        isGameFinish = true;
      }
      return {
        [`${turn}Score`]: state[`${turn}Score`] + 1,
        isGameFinish
      };
    });
  };

  changeTurnHandler = nextTurn => {
    this.setState({
      turn: nextTurn
    });
  };

  toggleShowModalHandler = () => {
    this.setState(state => ({
      showModal: !state.showModal
    }));
  };

  onChangeBlueName = e => {
    this.setState({ blueName: e.target.value });
  };

  onChangeRedName = e => {
    this.setState({ redName: e.target.value });
  };

  onStartGame = () => {
    this.setState({
      showModal: false,
      start: true
    });
  };

  handleSaveToLocalStorage = () => {
    const lastData = localStorage.getItem("mda-point-line-game") || [];
    const data = {
      [this.state.blueName]: this.state.blueScore,
      [this.state.redName]: this.state.redScore
    };
    localStorage.setItem("mda-point-line-game", JSON.stringify(lastData.concat(data)));
  };

  render() {
    return (
      <Layout className="layout" style={{ display: "flex", minHeight: "100vh", flexDirection: "column" }}>
        <Header style={{ flexGrow: 0 }}>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]} style={{ lineHeight: "64px" }}>
            <Menu.Item key="1">بازی نقطه خط</Menu.Item>
          </Menu>
        </Header>
        <Content
          style={{ flexGrow: 1, position: "relative", display: "flex", justifyContent: "center", alignItems: "center" }}
        >
          <>
            {!this.state.start && (
              <Button type="primary" onClick={this.toggleShowModalHandler}>
                مشخصات بازیکنان
              </Button>
            )}
            <Modal
              title="مشخصات بازیکنان"
              visible={this.state.showModal}
              onOk={this.onStartGame}
              onCancel={this.toggleShowModalHandler}
            >
              <Input value={this.state.blueName} onChange={this.onChangeBlueName} placeholder="بازیکن شماره ۱" />
              <Input
                value={this.state.redName}
                onChange={this.onChangeRedName}
                style={{ marginTop: 10 }}
                placeholder="بازیکن شماره ۲"
              />
            </Modal>
          </>

          {this.state.start && (
            <Board
              turn={this.state.turn}
              rowCount={this.state.rowCount}
              colCount={this.state.colCount}
              pointSize={5}
              lineSize={35}
              changeTurnHandler={this.changeTurnHandler}
              countScoreHandler={this.countScoreHandler}
            />
          )}

          {
            <Modal title="خسته نباشید" visible={this.state.isGameFinish} onOk={this.handleSaveToLocalStorage}>
              <div>
                <span>{this.state.blueName}</span> <span>{this.state.blueScore}</span>
                <span>{this.state.redName}</span> <span>{this.state.redScore}</span>
              </div>
            </Modal>
          }
        </Content>
        <Footer style={{ textAlign: "center", flexGrow: 0 }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    );
  }
}

export default App;
