import React, { Component } from "react";

class Point extends Component {
    render() {
        const { y, x } = this.props;
        return (
            <div
                className={"point"}
                style={{ top: `${y}px`, left: ` ${x}px` }}
            >
                
            </div>
        );
    }
}

export default Point;
