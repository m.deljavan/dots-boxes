import React, {Component} from 'react';
import Square from './Square';

class Squares extends Component {
  render() {
    return (
      <div>
        {this.props.squares.map(row =>
          row.map(({ key, type, x, y, backgroundColor }) => (
            <Square key={key} backgroundColor={backgroundColor} type={type} x={x} y={y} />
          ))
        )}
        
      
      </div>
    )
  }
}

export default Squares;