import React, { Component } from "react";
import _ from "lodash";
import Lines from "./Lines";
import Squares from "./squares";
import Points from "./Points";

class Board extends Component {
  constructor(props) {
    super(props);

    const { rowCount, colCount } = props;

    this.state = {
      squares: this.createSquareBorders(rowCount, colCount),
      horizontalLines: this.createLinesProp(
        rowCount,
        colCount,
        "horizontal"
      ),
      verticalLines: this.createLinesProp(
        rowCount,
        colCount,
        "vertical"
      )
    };
  }

  createSquareBorders = (rowCount, colCount) => {
    const { lineSize, pointSize } = this.props;
    const squares = [];
    const squareBorders = {
      top: null,
      right: null,
      bottom: null,
      left: null,
      backgroundColor: "",
      key: "",
      x: 0,
      y: 0
    };

    for (let row = 0; row < rowCount; row++) {
      squares[row] = [];
      for (let col = 0; col < colCount; col++) {
        squares[row].push({
          ...squareBorders,
          key: "" + row + col,
          y: row * lineSize + pointSize,
          x: col * lineSize + pointSize
        });
      }
    }

    return squares;
  };

  createLinesProp = (rowCount, colCount, type) => {
    const lines = [];
    const { lineSize, pointSize } = this.props;
    const lineProps = {
      classNames: ["not-set"],
      x: 0,
      y: 0,
      type,
      key: ""
    };

    rowCount =
      type === "vertical" ? rowCount : rowCount + 1;
    colCount =
      type === "vertical" ? colCount + 1 : colCount;

    for (let row = 0; row < rowCount; row++) {
      lines[row] = [];
      for (let col = 0; col < colCount; col++) {
        lines[row].push({
          ...lineProps,
          classNames: [...lineProps.classNames],
          key: "" + row + col,
          y:
            row * lineSize +
            (type === "vertical" ? pointSize : 0),
          x:
            col * lineSize +
            (type === "vertical" ? 0 : pointSize)
        });
      }
    }

    return lines;
  };
  onClickLineHandler = (row, col, type) => {
    let lines = [];

    if (type === "horizontal") {
      lines = _.cloneDeep(this.state.horizontalLines);
    } else {
      lines = _.cloneDeep(this.state.verticalLines);
    }
    const { turn } = this.props;

    if (!lines[row][col].classNames.includes("not-set")) {
      return;
    }
    const notSetClassIndex = lines[row][
      col
    ].classNames.indexOf("not-set");
    console.log(lines[row][col]);
    
    lines[row][col].classNames.splice(notSetClassIndex, 1);
    lines[row][col].classNames.push(turn);

    if (type === "horizontal") {
      this.clickHorizontalLineHandler(row, col);
    } else if (type === "vertical") {
      this.clickVerticalLineHandler(row, col);
    }
    this.setState({
      [type === "horizontal"
        ? "horizontalLines"
        : "verticalLines"]: lines
    });
  };

  clickHorizontalLineHandler = (row, col) => {
    const { turn, rowCount } = this.props;
    const squares = _.cloneDeep(this.state.squares);

    if (row !== 0) {
      squares[row - 1][col].bottom = turn;
    }

    if (row !== rowCount) {
      squares[row][col].top = turn;
    }

    this.setState({ squares });
    this.checkMakeSquare(squares);
  };

  clickVerticalLineHandler = (row, col) => {
    const { turn, colCount } = this.props;

    const squares = _.cloneDeep(this.state.squares);

    if (col !== 0) {
      squares[row][col - 1].right = turn;
    }

    if (col !== colCount) {
      squares[row][col].left = turn;
    }

    this.setState({ squares });
    this.checkMakeSquare(squares);
  };

  checkMakeSquare = squares => {
    const {
      rowCount,
      colCount,
      turn,
      changeTurnHandler,
      countScoreHandler
    } = this.props;

    let changeTurn = true;

    for (let row = 0; row < rowCount; row++) {
      for (let col = 0; col < colCount; col++) {
        if (squares[row][col].backgroundColor) {
          continue;
        }

        if (
          squares[row][col].top &&
          squares[row][col].right &&
          squares[row][col].bottom &&
          squares[row][col].left
        ) {
          countScoreHandler(turn);
          changeTurn = false;
          squares[row][col].backgroundColor = turn;
        }
      }
    }

    if (changeTurn) {
      const nextTurn = turn === "blue" ? "red" : "blue";
      changeTurnHandler(nextTurn);
    }

    this.setState({ squares });
  };

  render() {
    const { rowCount, colCount, lineSize } = this.props;

    return (
      <div>
        <Points
          rowCount={rowCount}
          colCount={colCount}
          lineSize={lineSize}
        />
        <Lines
          verticalLines={this.state.verticalLines}
          horizontalLines={this.state.horizontalLines}
          onClickLineHandler={this.onClickLineHandler}
        />
        <Squares squares={this.state.squares} />
      </div>
    );
  }
}

export default Board;
