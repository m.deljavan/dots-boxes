import React, {Component} from 'react';
import Point from './Point';

class Points extends Component {
  render() {
    const {rowCount, colCount, lineSize} = this.props;
    const pointElements = [];

    /** generate points */
  for (let row = 0; row < rowCount + 1; row++) {
    for (let col = 0; col < colCount + 1; col++) {
      pointElements.push(<Point key={"0" + row + col} y={row * lineSize} x={col * lineSize} />);
    }
  }
    return pointElements;
  }
}

export default Points;