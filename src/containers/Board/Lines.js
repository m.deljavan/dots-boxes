import React, { Component } from "react";
import Line from "./Line";

class Lines extends Component {
  render() {
    
    const { horizontalLines, verticalLines, onClickLineHandler } = this.props;
    console.log(verticalLines);
    return (
      <>
        {horizontalLines.map((row, rowIndex) =>
          row.map(({ key, type, x, y, classNames }, colIndex) => (
            <Line key={key} classNames={classNames} type={type} x={x} y={y} rowIndex={rowIndex} colIndex={colIndex} onClickLineHandler={onClickLineHandler}/>
          ))
        )}

        {verticalLines.map((row, rowIndex) =>
          row.map(({ key, type, x, y, classNames }, colIndex) => (
            <Line key={key} classNames={classNames} type={type} rowIndex={rowIndex} colIndex={colIndex} x={x} y={y} onClickLineHandler={onClickLineHandler}/>
          ))
        )}
      </>
    );
  }
}

export default Lines;
