import React, { Component } from "react";

class Square extends Component {
    render() {
        const {x, y, backgroundColor} = this.props;
        return (
            <div
                className={`square ${backgroundColor}`}
                style={{ top: `${y}px`, left: `${x}px`}}
            ></div>
        );
    }
}

export default Square;
