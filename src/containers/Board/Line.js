import React, { Component } from "react";

class Line extends Component {
  render() {
    const { x, y, onClickLineHandler, classNames, type, rowIndex, colIndex } = this.props;
    
    return (
      <div
        className={`line line-${type} ${classNames.join(" ")}`}
        style={{ top: `${y}px`, left: `${x}px` }}
        onClick={() => onClickLineHandler(rowIndex, colIndex, type)}
      />
    );
  }
}

export default Line;
